package com.example.rambo.explorer.zz_config.aa_status_activity;

import android.app.ActivityManager;
import android.util.Log;

import java.util.List;


public class _static_activity_status {

    public static ActivityManager activityManager;

    public static String getAppTaskState(){



        StringBuilder stringBuilder=new StringBuilder();
        int totalNumberOfTasks=_static_activity_status.activityManager.getRunningTasks(10).size();//Returns total number of tasks - stacks
        stringBuilder.append("\nTotal Number of Tasks: "+totalNumberOfTasks+"\n\n");

        List<ActivityManager.RunningTaskInfo> taskInfo =_static_activity_status.activityManager.getRunningTasks(10);//returns List of RunningTaskInfo - corresponding to tasks - stacks

        for(ActivityManager.RunningTaskInfo info:taskInfo)
        {


            //Log.d("hello", String.valueOf(info.describeContents()));


            stringBuilder.append("Task Id: "+info.id+", Number of Activities : "+info.numActivities+"\n");//Number of Activities in task - stack

            // Display the root Activity of task-stack
            stringBuilder.append("TopActivity: "+info.topActivity.getClassName().
                    toString().replace("com.example.rambo.explorer.","")+"\n");

            // Display the top Activity of task-stack
            stringBuilder.append("BaseActivity:"+info.baseActivity.getClassName().
                    toString().replace("com.example.rambo.explorer.","")+"\n");
            stringBuilder.append("\n\n");
        }
        return stringBuilder.toString();
    }



}
