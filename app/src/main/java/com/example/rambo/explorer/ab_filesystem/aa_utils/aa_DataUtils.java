package com.example.rambo.explorer.ab_filesystem.aa_utils;

public class aa_DataUtils {

    private static class DataUtilsHolder
    {
        private static final aa_DataUtils INSTANCE = new aa_DataUtils();
    }

    public static aa_DataUtils getInstance()
    {
        return DataUtilsHolder.INSTANCE;
    }

}
