package com.example.rambo.explorer.aa_acitivities;

import android.app.ActivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;

import com.example.rambo.explorer.R;
import com.example.rambo.explorer.ab_filesystem.aa_utils.aa_DataUtils;
import com.example.rambo.explorer.zz_config.aa_Config;
import com.example.rambo.explorer.zz_config.aa_status_activity._static_activity_status;

public class aa_MainActivity extends AppCompatActivity
{




    private aa_DataUtils aaDataUtils;

    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if(aa_Config.TEST)
        {
            if (_static_activity_status.activityManager == null)
            {
                _static_activity_status.activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
            }

        }
        aaDataUtils = aa_DataUtils.getInstance();
        setContentView(R.layout.activity_main);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {

        if(aa_Config.TEST)
        {

            switch (keyCode)
            {
                case KeyEvent.KEYCODE_A:
                {

                    Log.d("hello", _static_activity_status.getAppTaskState());
                    //your Action code
                    return true;
                }
            }

        }


        return super.onKeyDown(keyCode, event);
    }
}
